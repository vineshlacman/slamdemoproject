﻿using MathNet.Numerics.Statistics;
using SALMTesting.SLAMFiles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SALMTesting
{
    public static class SLAMMath
    {
        public static bool FindDelay(double[] signal1, double[] signal2, out int delay, int minimumPointsToMatch = 1000)
        {
            delay = 0;
            if (signal1 == null || signal2 == null)
            {
                return false;
            }
            if (minimumPointsToMatch < 1)
            {
                return false;
            }
            if (signal1.Length < minimumPointsToMatch || signal2.Length < minimumPointsToMatch)
            {
                return false;
            }

            double[] biggerArray;
            double[] smallerArray;
            bool signal2BiggerThanSignal1 = signal2.Length > signal1.Length;
            if (signal2BiggerThanSignal1)
            {
                biggerArray = signal2;
                smallerArray = signal1;
            }
            else
            {
                biggerArray = signal1;
                smallerArray = signal2;
            }

            int totalLoops = smallerArray.Length + biggerArray.Length - (2 * minimumPointsToMatch - 1);
            double[] correlationCoefficients = new double[totalLoops];
            int correlationIndex = 0;

            int loopsWithPartialArray = smallerArray.Length - minimumPointsToMatch;

            double[] biggerArraySearchBlock;
            double[] smallerArraySearchBlock;

            int arraySize = minimumPointsToMatch;
            for (int i = 0; i < loopsWithPartialArray; i++, arraySize++)
            {
                biggerArraySearchBlock = new double[arraySize];
                smallerArraySearchBlock = new double[arraySize];
                Array.Copy(biggerArray, 0, biggerArraySearchBlock, 0, arraySize);
                Array.Copy(smallerArray, smallerArray.Length - arraySize, smallerArraySearchBlock, 0, arraySize);
                correlationCoefficients[correlationIndex++] = Correlation.Pearson(biggerArraySearchBlock, smallerArraySearchBlock);
            }
            int loopsWithAllOfSmallArray = totalLoops - 2 * loopsWithPartialArray;
            if (biggerArray.Length == smallerArray.Length)
            {
                correlationCoefficients[correlationIndex++] = Correlation.Pearson(biggerArray, smallerArray);
            }
            else
            {
                biggerArraySearchBlock = new double[smallerArray.Length];
                for (int i = 0; i < loopsWithAllOfSmallArray; i++)
                {
                    Array.Copy(biggerArray, i, biggerArraySearchBlock, 0, smallerArray.Length);
                    correlationCoefficients[correlationIndex++] = Correlation.Pearson(biggerArraySearchBlock, smallerArray);
                }
            }
            arraySize = smallerArray.Length - 1;
            for (int i = 0; i < loopsWithPartialArray; i++, arraySize--)
            {
                biggerArraySearchBlock = new double[arraySize];
                smallerArraySearchBlock = new double[arraySize];
                Array.Copy(biggerArray, biggerArray.Length - arraySize, biggerArraySearchBlock, 0, arraySize);
                Array.Copy(smallerArray, 0, smallerArraySearchBlock, 0, arraySize);
                correlationCoefficients[correlationIndex++] = Correlation.Pearson(biggerArraySearchBlock, smallerArraySearchBlock);
            }
            int offset = signal2BiggerThanSignal1 ? -loopsWithPartialArray : loopsWithPartialArray;

            delay = correlationCoefficients.ToList().IndexOf(correlationCoefficients.Max()) - loopsWithPartialArray;
            if (!signal2BiggerThanSignal1)
            {
                delay = -delay;
            }
            return true;
        }

        public static double[] HeadingFromQuaternions(double[] quaternionW,
                                                double[] quaternionX,
                                                double[] quaternionY,
                                                double[] quaternionZ,
                                                int pointsToSkipAtStart = 3999)
        {
            double[] heading = null;
            if (quaternionW == null || quaternionX == null || quaternionY == null || quaternionZ == null)
            {
                return null;
            }
            if (pointsToSkipAtStart < 0)
            {
                return null;
            }
            int numberOfWPoints = quaternionW.Length;
            if (numberOfWPoints <= pointsToSkipAtStart)
            {
                return null;
            }
            if (quaternionX.Length != numberOfWPoints || quaternionY.Length != numberOfWPoints || quaternionZ.Length != numberOfWPoints)
            {
                return null;
            }

            int pointsInHeading = numberOfWPoints - pointsToSkipAtStart;
            heading = new double[pointsInHeading];

            for (int i = 0, j = pointsToSkipAtStart; i < pointsInHeading; i++, j++)
            {
                double tanYValue = 2.0 * (quaternionX[j] * quaternionY[j] + quaternionW[j] * quaternionZ[j]);
                double tanXValue = Math.Pow(quaternionW[j], 2) + Math.Pow(quaternionX[j], 2)
                                 - Math.Pow(quaternionY[j], 2) - Math.Pow(quaternionZ[j], 2);
                heading[i] = -RadiansToDegrees(Math.Atan2(tanYValue, tanXValue));
            }
            return heading;
        }


        public static double[] PitchFromQuaternions(double[] quaternionW,
                                              double[] quaternionX,
                                              double[] quaternionY,
                                              double[] quaternionZ,
                                              int pointsToSkipAtStart = 3999)
        {
            double[] pitch = null;
            if (quaternionW == null || quaternionX == null || quaternionY == null || quaternionZ == null)
            {
                return null;
            }
            if (pointsToSkipAtStart < 0)
            {
                return null;
            }
            int numberOfWPoints = quaternionW.Length;
            if (numberOfWPoints <= pointsToSkipAtStart)
            {
                return null;
            }
            if (quaternionX.Length != numberOfWPoints || quaternionY.Length != numberOfWPoints || quaternionZ.Length != numberOfWPoints)
            {
                return null;
            }

            int pointsInPitch = numberOfWPoints - pointsToSkipAtStart;
            pitch = new double[pointsInPitch];

            for (int i = 0, j = pointsToSkipAtStart; i < pointsInPitch; i++, j++)
            {
                double angle = -2.0 * (quaternionX[j] * quaternionZ[j] - quaternionW[j] * quaternionY[j]);
                pitch[i] = RadiansToDegrees(Math.Asin(angle));
            }
            return pitch;
        }

        public static double RadiansToDegrees(double angle)
        {
            return angle * (180.0 / Math.PI);
        }

        public static double[] CoercePlusMinus360ToPlusMinus180(double[] inputArray)
        {
            double[] outputArray = null;

            if (inputArray == null)
            {
                return null;
            }
            int numberOfPoints = inputArray.Length;
            if (numberOfPoints == 0)
            {
                return new double[0];
            }

            outputArray = new double[numberOfPoints];

            for (int i = 0; i < numberOfPoints; i++)
            {
                if (inputArray[i] > 180.0)
                {
                    outputArray[i] = inputArray[i] - 360.0;
                }
                else if (inputArray[i] < -180.0)
                {
                    outputArray[i] = inputArray[i] + 360.0;
                }
                else
                {
                    outputArray[i] = inputArray[i];
                }
            }
            return outputArray;
        }


        public static void Rotate(ICollection<Point3D> trajectory, double rotation)
        {
            double cosRotation = Math.Cos(rotation);
            double sinRotation = Math.Sin(rotation);
            Point3D firstPoint = trajectory.First();
            Parallel.ForEach(trajectory, point =>
            {
                double x = cosRotation * (point.X - firstPoint.X) - sinRotation * (point.Y - firstPoint.Y) + firstPoint.X;
                double y = sinRotation * (point.X - firstPoint.X) + cosRotation * (point.Y - firstPoint.Y) + firstPoint.Y;

                point.X = x;
                point.Y = y;
            });
        }
    }
}
