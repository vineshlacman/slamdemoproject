﻿using SALMTesting.SLAMFiles;
using System;
using System.IO;
using System.Windows.Forms;

namespace SALMTesting
{
    public partial class MainForm : Form
    {
        TaskRobinPOS m_RobinPOS;
        TaskRevoPOS m_revoPos;
        public MainForm()
        {
            InitializeComponent();
            m_RobinPOS = new TaskRobinPOS();
            m_revoPos = new TaskRevoPOS();
        }

        void RobinBrowseButtonClick(object sender, EventArgs e)
        {
            m_TextBoxFilePath.Text = m_RobinPOS.GetPath();
        }
        void RevoBrowseButtonClick(object sender, EventArgs e)
        {
            textBox1.Text = m_revoPos.GetPath();
        }

        void RobinReadButtonCLick(object sender, EventArgs e)
        {
            m_RobinPOS.ReadFile(new FileInfo(m_TextBoxFilePath.Text), (int)m_NumericUpDownRecordToSkip.Value, RobinReceivedString);
            m_RobinProgressBar.Minimum = 0;
            m_RobinProgressBar.Maximum = (int)m_RobinPOS.TrjFile.FileSize;
        }

        void RevoReadButtonClick(object sender, EventArgs e)
        {
            m_revoPos.ReadFile(new FileInfo(textBox1.Text), (int)m_NumericUpDownRecordToSkip.Value, RevoReceivedString);
            m_RevoProgressbar.Minimum = 0;
            m_RevoProgressbar.Maximum = (int)m_revoPos.TrjFile.FileSize; ;
        }

        void RobinReceivedString(string record)
        {
            Invoke(new Action(() =>
            {
                m_RobinProgressBar.Value = (int)(m_RobinPOS.TrjFile.getFilePosition());
            }));
        }
        void RevoReceivedString(string record)
        {
            Invoke(new Action(() =>
            {
                m_RevoProgressbar.Value = (int)(m_revoPos.TrjFile.getFilePosition());
                
            }));
           
        }
        
        void MainFormFormClosed(object sender, FormClosedEventArgs e)
        {
            m_revoPos.Dispose();
            m_RobinPOS.Dispose();
        }
    }
}
