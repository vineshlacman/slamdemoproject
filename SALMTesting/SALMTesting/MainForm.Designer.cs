﻿namespace SALMTesting
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.m_NumericUpDownRecordToSkip = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.Readrevo = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.m_BrowseRevo = new System.Windows.Forms.Button();
            this.m_ButtonRead = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.m_TextBoxFilePath = new System.Windows.Forms.TextBox();
            this.m_Browse = new System.Windows.Forms.Button();
            this.m_RichTextBox = new System.Windows.Forms.RichTextBox();
            this.m_RevoProgressbar = new System.Windows.Forms.ProgressBar();
            this.m_RobinProgressBar = new System.Windows.Forms.ProgressBar();
            this.RevoProcessinglabel = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_NumericUpDownRecordToSkip)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RevoProcessinglabel);
            this.panel1.Controls.Add(this.m_RobinProgressBar);
            this.panel1.Controls.Add(this.m_RevoProgressbar);
            this.panel1.Controls.Add(this.m_NumericUpDownRecordToSkip);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.Readrevo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.textBox1);
            this.panel1.Controls.Add(this.m_BrowseRevo);
            this.panel1.Controls.Add(this.m_ButtonRead);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.m_TextBoxFilePath);
            this.panel1.Controls.Add(this.m_Browse);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1088, 167);
            this.panel1.TabIndex = 6;
            // 
            // m_NumericUpDownRecordToSkip
            // 
            this.m_NumericUpDownRecordToSkip.Location = new System.Drawing.Point(136, 83);
            this.m_NumericUpDownRecordToSkip.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.m_NumericUpDownRecordToSkip.Name = "m_NumericUpDownRecordToSkip";
            this.m_NumericUpDownRecordToSkip.Size = new System.Drawing.Size(120, 22);
            this.m_NumericUpDownRecordToSkip.TabIndex = 9;
            this.m_NumericUpDownRecordToSkip.Value = new decimal(new int[] {
            4000,
            0,
            0,
            0});
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(64, 88);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Record To Skip";
            // 
            // Readrevo
            // 
            this.Readrevo.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Readrevo.Location = new System.Drawing.Point(632, 51);
            this.Readrevo.Name = "Readrevo";
            this.Readrevo.Size = new System.Drawing.Size(100, 29);
            this.Readrevo.TabIndex = 6;
            this.Readrevo.Text = "Read";
            this.Readrevo.UseVisualStyleBackColor = true;
            this.Readrevo.Click += new System.EventHandler(this.RevoReadButtonClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(14, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 17);
            this.label2.TabIndex = 7;
            this.label2.Text = "Revo File Path:";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(136, 54);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(371, 23);
            this.textBox1.TabIndex = 4;
            // 
            // m_BrowseRevo
            // 
            this.m_BrowseRevo.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_BrowseRevo.Location = new System.Drawing.Point(513, 51);
            this.m_BrowseRevo.Name = "m_BrowseRevo";
            this.m_BrowseRevo.Size = new System.Drawing.Size(100, 29);
            this.m_BrowseRevo.TabIndex = 5;
            this.m_BrowseRevo.Text = "Browse";
            this.m_BrowseRevo.UseVisualStyleBackColor = true;
            this.m_BrowseRevo.Click += new System.EventHandler(this.RevoBrowseButtonClick);
            // 
            // m_ButtonRead
            // 
            this.m_ButtonRead.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_ButtonRead.Location = new System.Drawing.Point(632, 11);
            this.m_ButtonRead.Name = "m_ButtonRead";
            this.m_ButtonRead.Size = new System.Drawing.Size(100, 29);
            this.m_ButtonRead.TabIndex = 2;
            this.m_ButtonRead.Text = "Read";
            this.m_ButtonRead.UseVisualStyleBackColor = true;
            this.m_ButtonRead.Click += new System.EventHandler(this.RobinReadButtonCLick);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(14, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 17);
            this.label1.TabIndex = 3;
            this.label1.Text = "Robin File Path:";
            // 
            // m_TextBoxFilePath
            // 
            this.m_TextBoxFilePath.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_TextBoxFilePath.Location = new System.Drawing.Point(136, 14);
            this.m_TextBoxFilePath.Name = "m_TextBoxFilePath";
            this.m_TextBoxFilePath.Size = new System.Drawing.Size(371, 23);
            this.m_TextBoxFilePath.TabIndex = 0;
            // 
            // m_Browse
            // 
            this.m_Browse.Font = new System.Drawing.Font("Tahoma", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.m_Browse.Location = new System.Drawing.Point(513, 11);
            this.m_Browse.Name = "m_Browse";
            this.m_Browse.Size = new System.Drawing.Size(100, 29);
            this.m_Browse.TabIndex = 1;
            this.m_Browse.Text = "Browse";
            this.m_Browse.UseVisualStyleBackColor = true;
            this.m_Browse.Click += new System.EventHandler(this.RobinBrowseButtonClick);
            // 
            // m_RichTextBox
            // 
            this.m_RichTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.m_RichTextBox.Location = new System.Drawing.Point(0, 0);
            this.m_RichTextBox.Name = "m_RichTextBox";
            this.m_RichTextBox.Size = new System.Drawing.Size(1088, 364);
            this.m_RichTextBox.TabIndex = 7;
            this.m_RichTextBox.Text = "";
            // 
            // m_RevoProgressbar
            // 
            this.m_RevoProgressbar.Location = new System.Drawing.Point(739, 53);
            this.m_RevoProgressbar.Name = "m_RevoProgressbar";
            this.m_RevoProgressbar.Size = new System.Drawing.Size(337, 23);
            this.m_RevoProgressbar.TabIndex = 10;
            // 
            // m_RobinProgressBar
            // 
            this.m_RobinProgressBar.Location = new System.Drawing.Point(739, 14);
            this.m_RobinProgressBar.Name = "m_RobinProgressBar";
            this.m_RobinProgressBar.Size = new System.Drawing.Size(337, 23);
            this.m_RobinProgressBar.TabIndex = 11;
            // 
            // RevoProcessinglabel
            // 
            this.RevoProcessinglabel.AutoSize = true;
            this.RevoProcessinglabel.Location = new System.Drawing.Point(739, 87);
            this.RevoProcessinglabel.Name = "RevoProcessinglabel";
            this.RevoProcessinglabel.Size = new System.Drawing.Size(46, 17);
            this.RevoProcessinglabel.TabIndex = 12;
            this.RevoProcessinglabel.Text = "label4";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1088, 364);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.m_RichTextBox);
            this.Name = "MainForm";
            this.Text = "Form1";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainFormFormClosed);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.m_NumericUpDownRecordToSkip)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.NumericUpDown m_NumericUpDownRecordToSkip;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button Readrevo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button m_BrowseRevo;
        private System.Windows.Forms.Button m_ButtonRead;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox m_TextBoxFilePath;
        private System.Windows.Forms.Button m_Browse;
        private System.Windows.Forms.RichTextBox m_RichTextBox;
        private System.Windows.Forms.ProgressBar m_RevoProgressbar;
        private System.Windows.Forms.ProgressBar m_RobinProgressBar;
        private System.Windows.Forms.Label RevoProcessinglabel;
    }
}

