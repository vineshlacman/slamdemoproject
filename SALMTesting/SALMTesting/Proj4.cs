﻿using System;
using System.Runtime.InteropServices;

namespace SALMTesting
{
    public class Proj4
    {
        [DllImport(".\\proj.dll", EntryPoint = "pj_init_plus")]
        public static extern IntPtr InitPlus(String str);

        [DllImport(".\\proj.dll", EntryPoint = "pj_transform")]
        public static extern int Transform(IntPtr src,
                                                IntPtr dst,
                                                int pointCount,
                                                int pointOffset,
                                                ref double X,
                                                ref double Y,
                                                ref double Z);

        [DllImport(".\\proj.dll", EntryPoint = "pj_free")]
        public static extern void Dispose(IntPtr proj4Object);

        public static int getUTMZone(double longitude)
        {
            double temp = longitude + 180.0 - (int)((longitude + 180.0) / 360.0) * 360 - 180.0;
            return (int)((temp + 180.0) / 6.0) + 1;
        }

        public static double DegToRad(double deg)
        {
            return ((deg * Math.PI) / 180.0);
        }

        public static double RadToDeg(double rad)
        {
            return rad * (180.0 / Math.PI);
        }

        public static bool CheckCoordinateSystem(String str)
        {
            return Proj4.InitPlus(str) != IntPtr.Zero;
        }

        public static String GetLatLongString()
        {
            String str = "+proj=latlong +datum=WGS84";
            //if (IntPtr.Size != 4)
            //str = str.Replace(" ", "");
            return str;
        }

        public static IntPtr InitUTMCoordinateSystem(double InitialLatitude, double InitialLongitude)
        {
            String str = String.Format("+proj=utm +ellps=WGS84 {0}+zone={1} +units=m +datum=WGS84", InitialLatitude < 0 ? "+south " : "", getUTMZone(InitialLongitude));
            if (IntPtr.Size != 4)
                str = str.Replace(" ", "");
            return InitPlus(str);
        }
    }
}
