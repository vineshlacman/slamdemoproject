﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SALMTesting.SLAMFiles
{
    public class TaskRobinPOS : IDisposable
    {
        Action<string> m_ReceivedString;
        OpenFileDialog m_OpenFileDialog;
        TrajectoryFileRobinPos m_TrjFile;
        FileInfo m_filePath;
        int m_RecordToSkip;
        Thread m_ProcessingThread;
        public TaskRobinPOS()
        {
            m_OpenFileDialog = new OpenFileDialog();
            m_OpenFileDialog.Filter = "POS Files|*.pos";
            m_TrjFile = null;
        }

        public string GetPath()
        {
            if (!(m_OpenFileDialog.ShowDialog() == DialogResult.OK))
            {
                return "";
            }
            return m_OpenFileDialog.FileName;

        }
        public TrajectoryFileRobinPos TrjFile
        {
            get { return m_TrjFile; }
        }
        internal void ReadFile(FileInfo filePath, int recordtoskip, Action<string> receivedString)
        {
            m_ReceivedString = receivedString;
            m_RecordToSkip = recordtoskip;
            m_filePath = filePath;
            m_TrjFile = new TrajectoryFileRobinPos(m_filePath.FullName, m_RecordToSkip);
            m_ProcessingThread = new Thread(Process);
            m_ProcessingThread.Start();
        }

        void Process()
        {

            if (!m_filePath.Exists)
            {
                throw new Exception("Invalid Robin file");
            }

            string robinPosString = "";
            string outputFileName = "RobinOutputFile.pos";
            m_TrjFile = new TrajectoryFileRobinPos(m_filePath.FullName, m_RecordToSkip);
            bool eof = false;
            double initialLatitude = Convert.ToDouble(m_TrjFile.InitialRecord[0]);
            double initialLongitude = Convert.ToDouble(m_TrjFile.InitialRecord[2]);
            IntPtr srcCoord = Proj4.InitPlus("+proj=latlong +datum=WGS84");
            IntPtr dstCoord = Proj4.InitPlus("+proj=utm +ellps=WGS84 "
                                + (initialLatitude < 0 ? "+south " : "")
                                + "+zone=" + Proj4.getUTMZone(initialLongitude) + " +units=m +datum=WGS84");
         
            m_TrjFile.SetStartAndEndTime();
            double previousDx = 0;
            double previousDy = 0;
            StreamWriter outputFile = new StreamWriter("D:\\" + outputFileName);          

            while (!eof)
            {
                int recsRead = m_TrjFile.getData();
                eof = recsRead == 0;
                for (int i = 0; i < recsRead; i++)
                {
                    RobinPosFileObject robinObject = m_TrjFile.getRobinPosFileObject(i);
                    double x = Proj4.DegToRad(robinObject.Position.X);
                    double y = Proj4.DegToRad(robinObject.Position.Y);
                    double z = 0;
                    int err = Proj4.Transform(srcCoord, dstCoord, 1, 1, ref x, ref y, ref z);
                    robinObject.Position.X = x;
                    robinObject.Position.Y = y;
                    robinObject.SetDx((x - previousDx));
                    robinObject.SetDy((y - previousDy));
                    robinObject.SetRotation(Math.Atan2(robinObject.Dx, robinObject.Dy));
                    robinPosString = string.Format("RobinString : {0} {1} {2} {3} {4} {5} {6} dx={7} dy={8} rotation={9}", robinObject.T,
                        robinObject.Position.X, robinObject.Position.Y, robinObject.Position.Z,
                        robinObject.Roll, robinObject.Pitch, robinObject.Heading, robinObject.Dx, robinObject.Dy,robinObject.Rotation);
                    previousDx = robinObject.Position.X;
                    previousDy = robinObject.Position.Y;

                    outputFile.WriteLine(robinPosString);
                    m_ReceivedString(robinPosString);                  
                }
            }
            outputFile.Close();
        }

        public void Dispose()
        {
            if (m_ProcessingThread != null)
            {
                m_ProcessingThread.Abort();
            }
        }
    }
}
