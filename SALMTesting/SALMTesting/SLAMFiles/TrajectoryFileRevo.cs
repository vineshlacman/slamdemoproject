﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace SALMTesting.SLAMFiles
{
    public class TrajectoryFileRevo : TrajectoryFile
    {
        const int s_InverseValue = -1;
       FileInfo m_StreamFileInfo;
        int m_InitialRecordToSkipInStream;
        FieldSeparatedFile m_file;
        string[] m_posLines;

        public TrajectoryFileRevo(string path,int recordToSkip) : base(path)
        {
            m_StreamFileInfo = new FileInfo(path);
            m_InitialRecordToSkipInStream = recordToSkip;
            m_file = new FieldSeparatedFile(m_StreamFileInfo, ' ', m_InitialRecordToSkipInStream);           
            m_posLines = new string[MAX_RECORDS];         
            SetStartAndEndTime();
        }

        public override void SetStartAndEndTime()
        {
            InitialRecord = m_file.FirstRecordInFile;
            m_startTime = m_file.FirstRecordInFile[0];
            m_endTime = m_file.LastRecordInFile[0];
        }


        public override string getLine(int index)
        {
            return m_posLines[index];
        }

        protected override void resetFile()
        {
            m_file = new FieldSeparatedFile(m_StreamFileInfo, ' ', m_InitialRecordToSkipInStream);
            m_numRecords = 0;
        }

        public override ulong getFilePosition()
        {
            return m_file.GetStreamPosition();
        }

        public override int readData(SeekOrigin position)
        {
            if (position == SeekOrigin.End)
                return 0;

            int recnum = 0;
            int numread = 0;
            string lastTime = "";
            if (position == SeekOrigin.Begin)
            {
                resetFile();
               
            }
            if (position == SeekOrigin.Current && m_numRecords > 0)
            {
                m_storage[0] = m_storage[m_numRecords - 1];
                m_posLines[0] = m_posLines[m_numRecords - 1];
                recnum++;
            }
            bool end = m_file.IsEndOfFile;
            while (recnum < MAX_RECORDS)
            {
              
                List<string> recordParameter = m_file.ReadNextRecord();
                end = m_file.IsEndOfFile;
                if (recordParameter.Count() > 0)
                {
                   
                    try
                    {
                        if (m_firstTime == 0.0)
                        {
                            m_firstTime = Convert.ToDouble(recordParameter[0]);
                        }
                        if (StartTime == "")
                        {
                            StartTime = recordParameter[0];
                        }
                        m_posLines[recnum] = m_file.LastProcessedLine;
                        lastTime = recordParameter[0];

                        m_storage[recnum].RevoPosFileObject = new RevoPosFileObject(Convert.ToDouble(recordParameter[0]),
                            Convert.ToDouble(recordParameter[1]),
                            Convert.ToDouble(recordParameter[2]) * s_InverseValue,
                            Convert.ToDouble(recordParameter[3]),
                            Convert.ToDouble(recordParameter[4]),
                            Convert.ToDouble(recordParameter[5]),
                            Convert.ToDouble(recordParameter[6]),
                            Convert.ToDouble(recordParameter[7]));
                        recnum++;
                        numread++;
                    }
                    catch (Exception ex)
                    {
                        EndTime = lastTime;
                        Console.WriteLine("EXCEPTION: " + ex.Message);
                    }
                }
                if (end)
                {
                    EndTime = lastTime;
                    break;
                }
            }

            m_numRecords = recnum;
            return numread;
        }

        public override void Close()
        {
            m_file.Dispose();
            m_posLines = null;
        }

        public override void skipToTime(double time)
        {
            m_file.ReadNextRecord();
            string line = m_file.LastProcessedLine;
            int length = line.Length + 2;
            double gap;
            double point = 0;
            double position;
            bool eof = false;
            gap = m_storage[2].Time - m_storage[1].Time;
            point = (time - Convert.ToDouble(this.StartTime)) / gap;
            position = point * length;
            m_file.SetStreamPosition(Convert.ToInt64(position));
            m_file.ReadNextRecord();
            eof = m_file.IsEndOfFile;
            while (eof)
            {
                gap = gap * 1.5;
                point = (time - Convert.ToDouble(this.StartTime)) / gap;
                position = point * length;
                m_file.SetStreamPosition(Convert.ToInt64(position));
                m_file.ReadNextRecord();
                eof = m_file.IsEndOfFile;
            }
            readData(SeekOrigin.Current);
            Boolean accurate = false;
            while (!accurate)
            {
                if (m_storage[1].Time > time)
                {
                    point = (m_storage[1].Time - time) / gap;
                    position = position - (point * length);
                    m_file.SetStreamPosition(Convert.ToInt64(position));
                    m_file.ReadNextRecord();
                    eof = m_file.IsEndOfFile;
                    while (eof)
                    {
                        point = (m_storage[1].Time - time) / gap;
                        position = position - (point * length);
                        m_file.SetStreamPosition(Convert.ToInt64(position));
                        m_file.ReadNextRecord();
                        eof = m_file.IsEndOfFile;
                    }
                    readData(SeekOrigin.Current);
                }
                else if (m_storage[1].Time < time)
                {
                    point = (time - m_storage[1].Time) / gap;
                    position = position + (point * length);
                    m_file.SetStreamPosition(Convert.ToInt64(position));
                    m_file.ReadNextRecord();
                    eof = m_file.IsEndOfFile;
                    while (eof)
                    {
                        point = (time - m_storage[1].Time) / gap;
                        position = position + (point * length);
                        m_file.SetStreamPosition(Convert.ToInt64(position));
                        m_file.ReadNextRecord();
                        eof = m_file.IsEndOfFile;
                    }

                    readData(SeekOrigin.Current);
                }
                if (time - m_storage[1].Time < 1 && time - m_storage[1].Time >= 0)
                {
                    accurate = true;
                }
            }
        }
    }
}
