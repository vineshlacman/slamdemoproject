﻿namespace SALMTesting.SLAMFiles
{
    public struct TrajectoryParams
    {
        public double Time;
        public double Latitude;
        public double Longitude;
        public double Height;
        public double Heading;
        public double Roll;
        public double Pitch;

        public double YAccuracy;
        public double XAccuracy;
        public double ZAccuracy;

        public double PitchAccuracy;
        public double RollAccuracy;
        public double HeadingAccuracy;
        public RevoPosFileObject RevoPosFileObject;
        public RobinPosFileObject RobinPosFileObject;

        public TrajectoryParams(double time, double lat, double lon, double h, double heading, double roll, double pitch, double yAccuracy, double xAccuracy, double zAccuracy,
                                                double pitchAccuracy, double rollAccuracy, double headingAccuracy, RevoPosFileObject revoPosFileObject, RobinPosFileObject robinPosFileObject)
        {
            Time = time;
            Latitude = lat;
            Longitude = lon;
            Height = h;
            Heading = heading;
            Roll = roll;
            Pitch = pitch;
            YAccuracy = yAccuracy;
            XAccuracy = xAccuracy;
            ZAccuracy = zAccuracy;
            PitchAccuracy = pitchAccuracy;
            RollAccuracy = rollAccuracy;
            HeadingAccuracy = headingAccuracy;
            RevoPosFileObject = revoPosFileObject;
            RobinPosFileObject = robinPosFileObject;
        }
    }
}
