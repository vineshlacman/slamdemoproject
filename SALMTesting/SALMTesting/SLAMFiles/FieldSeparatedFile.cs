﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace SALMTesting.SLAMFiles
{
    class FieldSeparatedFile
    {
        static readonly char[] s_DefaultfDelimeters = { ',', '\t', ' ' };
        char m_Delimeter;
        FileStream m_FileStream;
        StreamReader m_StreamReader;
        FileInfo m_FileInfo;
        int m_NumberOfLineToSkip;

        public FieldSeparatedFile(FileInfo fileInfo, char delimeter, int skipLines)
        {
            m_FileInfo = fileInfo;
            m_NumberOfLineToSkip = skipLines;
            m_FileStream = new FileStream(fileInfo.FullName, FileMode.Open, FileAccess.Read);
            m_StreamReader = new StreamReader(m_FileStream);
            m_Delimeter = delimeter;
            SetStreamPosition(0);
            SetetLastLine();
            SetStreamPosition(0);
            for (int recordNumber = 0; recordNumber < m_NumberOfLineToSkip; recordNumber++)
            {
                ReadLine();
            }
            SetetFirstLine();
        }

        public string LastProcessedLine { get; private set; }

        public bool IsEndOfFile { get; private set; }

        public List<string> LastRecordInFile { get; private set; }

        public List<string> FirstRecordInFile { get; private set; }

        public List<string> ReadNextRecord()
        {
            return ReadLine();
        }

        public void SetStreamPosition(long position)
        {
            m_StreamReader.BaseStream.Position = position;
        }

        public void Dispose()
        {
            m_FileStream.Close();
            m_StreamReader.Close();
        }

        internal ulong GetStreamPosition() { return (ulong)m_StreamReader.BaseStream.Position; }

        List<string> ReadLine()
        {
            IsEndOfFile = m_StreamReader.EndOfStream;
            if (IsEndOfFile)
            {
                return new List<string>();
            }
            LastProcessedLine = m_StreamReader.ReadLine();
            LastProcessedLine = System.Text.RegularExpressions.Regex.Replace(LastProcessedLine, @"\s+", " ");
            if (!LastProcessedLine.Contains(m_Delimeter))
            {
                throw new Exception("Record not in valid formate" + LastProcessedLine);
            }
            IsEndOfFile = m_StreamReader.EndOfStream;
            return LastProcessedLine.Split(m_Delimeter).ToList();

        }

        void SetetFirstLine()
        {
            FirstRecordInFile = ReadLine();
        }

        void SetetLastLine()
        {
            while (!IsEndOfFile)
            {
                LastRecordInFile = ReadLine();
            }
        }

    }
}
