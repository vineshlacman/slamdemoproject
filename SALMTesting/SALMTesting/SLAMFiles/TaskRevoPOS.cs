﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace SALMTesting.SLAMFiles
{
    public class TaskRevoPOS : IDisposable
    {

        OpenFileDialog m_OpenFileDialog;
        TrajectoryFileRevo m_TrjFile;
        FileInfo m_filePath;
        int m_RecordToSkip;
        Thread m_ProcessingThread;
        Action<string> m_ReceivedString;
        public TaskRevoPOS()
        {
            m_OpenFileDialog = new OpenFileDialog();
            m_OpenFileDialog.Filter = "REVO Files|*.txt";
            m_TrjFile = null;

        }

        public string GetPath()
        {
            if (!(m_OpenFileDialog.ShowDialog() == DialogResult.OK))
            {
                return "";
            }
            return m_OpenFileDialog.FileName;

        }
        public TrajectoryFileRevo TrjFile
        {
            get { return m_TrjFile; }
        }
        internal void ReadFile(FileInfo filePath, int recordtoskip, Action<string> receivedString)
        {
            m_ReceivedString = receivedString;
            m_RecordToSkip = recordtoskip;
            m_filePath = filePath;
            m_TrjFile = new TrajectoryFileRevo(m_filePath.FullName, m_RecordToSkip);
            m_ProcessingThread = new Thread(Process);
            m_ProcessingThread.Start();
        }

        void Process()
        {

            if (!m_filePath.Exists)
            {
                throw new Exception("Invalid Robin file");
            }

            string revoString = "";
            string outputFileName = "RevoOutputFile.txt";
            bool eof = false;
            double previousDx = 0;
            double previousDy = 0;
            StreamWriter outputFile = new StreamWriter("D:\\" + outputFileName);

            while (!eof)
            {
                int recsRead = m_TrjFile.getData();
                eof = recsRead == 0;
                for (int i = 0; i < recsRead; i++)
                {
                    RevoPosFileObject tempObject = m_TrjFile.getRevoPosFileObject(i);
                    tempObject.SetDx((tempObject.Position.X - previousDx));
                    tempObject.SetDy((tempObject.Position.Y - previousDy));
                    tempObject.SetRotation(Math.Atan2(tempObject.Dx, tempObject.Dy));
                    revoString = string.Format("RevoString : {0} {1} {2} {3} {4} {5} {6} {7} dx={8} dy={9} rotation={10}", tempObject.T,
                      tempObject.Position.X, tempObject.Position.Y, tempObject.Position.Z,
                      tempObject.Q1,
                      tempObject.Q2, tempObject.Q3, tempObject.Q4, tempObject.Dx, tempObject.Dy, tempObject.Rotation);
                    previousDx = tempObject.Position.X;
                    previousDy = tempObject.Position.Y;
                    outputFile.WriteLine(revoString);
                    m_ReceivedString(revoString);
                }
            }
            outputFile.Close();
        }


        public void Dispose()
        {
            if (m_ProcessingThread != null)
            {
                m_ProcessingThread.Abort();
            }

        }
    }
}
