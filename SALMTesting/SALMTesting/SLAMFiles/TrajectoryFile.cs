﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

namespace SALMTesting.SLAMFiles
{
    public delegate void ProcessTrajectoryDelegate(ulong progress);

     public abstract class TrajectoryFile
    {     

        protected const int MAX_RECORDS = 8192;
        protected TrajectoryParams[] m_storage;
        protected List<TrajectoryParams> m_storageList;
        protected Int32 m_currRecordIdx;
        protected Int32 m_numRecords;
        private TrajectoryParams m_posDiff;
        protected double m_firstTime;
        protected String m_filePath;
        protected FileInfo m_fileInfo;
        protected String m_startTime, m_endTime;
        protected bool m_hasAccuracy;
        //This will track the orientation of the IMU for Swivel Mounts
        protected Int32 m_orientation;
        protected bool m_trackOrientation;
        protected TrajectoryParams m_lastTrackedPosition;
        private ProcessTrajectoryDelegate m_progressUpdate;
        protected double start_time_d = Double.NaN;

        public TrajectoryFile(string path)
        {
            m_storage = new TrajectoryParams[MAX_RECORDS];
            m_filePath = path;
            m_fileInfo = new FileInfo(path);
            m_startTime = m_endTime = "";
            m_hasAccuracy = false;
            m_trackOrientation = false;
            m_orientation = 0;
            m_lastTrackedPosition = new TrajectoryParams();
            m_storageList = new List<TrajectoryParams>();
            m_progressUpdate = null;
        }

        ~TrajectoryFile()
        {
            m_storage = null;
        }

        public ProcessTrajectoryDelegate ProgressUpdateDelegate
        {
            get { return m_progressUpdate; }
            set { m_progressUpdate = value; }
        }

        public abstract void Close();

        public abstract String getLine(int index);

        public abstract void SetStartAndEndTime();

        public List<string> InitialRecord { get; set; }
       
        public String StartTime
        {
            get { return m_startTime; }
            set { m_startTime = value; }
        }

        public String EndTime
        {
            get { return m_endTime; }
            set { m_endTime = value; }
        }

        public Double StartTimeD
        {
            get
            {
                if (Double.IsNaN(start_time_d)) start_time_d = Convert.ToDouble(StartTime);
                return start_time_d;
            }
        }

        public Boolean TrackOrientation
        {
            get { return m_trackOrientation; }
            set { m_trackOrientation = value; }
        }

        public Int32 Orientation
        {
            get { return m_orientation; }
        }

        public double getTime(int index)
        {
            return m_storage[index].Time;
        }

        public double getLatitude(int index)
        {
            return m_storage[index].Latitude;
        }

        public double getLongitude(int index)
        {
            return m_storage[index].Longitude;
        }

        public double getHeight(int index)
        {
            return m_storage[index].Height;
        }

        public double getHeading(int index)
        {
            return m_storage[index].Heading;
        }

        public double getRoll(int index)
        {
            return m_storage[index].Roll;
        }

        public double getPitch(int index)
        {
            return m_storage[index].Pitch;
        }

        public double getXAcc(int index)
        {
            return m_storage[index].XAccuracy;
        }

        public double getYAcc(int index)
        {
            return m_storage[index].YAccuracy;
        }

        public double getZAcc(int index)
        {
            return m_storage[index].ZAccuracy;
        }

        public double getPitchAcc(int index)
        {
            return m_storage[index].PitchAccuracy;
        }

        public double getRollAcc(int index)
        {
            return m_storage[index].RollAccuracy;
        }

        public double getHeadingAcc(int index)
        {
            return m_storage[index].HeadingAccuracy;
        }

        public RevoPosFileObject getRevoPosFileObject(int index)
        {
            return m_storage[index].RevoPosFileObject;
        }

        public RobinPosFileObject getRobinPosFileObject(int index)
        {
            return m_storage[index].RobinPosFileObject;
        }
        public Int32 getData()
        {
            return ReadRecords(SeekOrigin.Current);
        }

        public UInt64 FileSize
        {
            get { return (UInt64)m_fileInfo.Length; }
        }

        public abstract Int32 readData(SeekOrigin position);

        protected abstract void resetFile();

        public abstract UInt64 getFilePosition();

        public abstract void skipToTime(double time);

        public Int32 ReadRecords(SeekOrigin position)
        {
            Int32 recs = readData(position);
            m_storageList = m_storage.ToList();
            return recs;
        }

        public double getInitialLongitude()
        {
            ReadRecords(SeekOrigin.Begin);
            resetFile();
            return m_storage[0].Longitude;
        }

        public double getInitialHeight()
        {
            ReadRecords(SeekOrigin.Begin);
            resetFile();
            return m_storage[0].Height;
        }

        public double getInitialLatitude()
        {
            ReadRecords(SeekOrigin.Begin);
            resetFile();
            return m_storage[0].Latitude;
        }

        public double getInitialHeading()
        {
            ReadRecords(SeekOrigin.Begin);
            resetFile();
            return m_storage[0].Heading;
        }

        public bool getInterpolationValues(double Time, ref double Lat, ref double Lon, ref double Height, ref double Heading,
                                                        ref double Roll, ref double Pitch, ref double YAccuracy, ref double XAccuracy, ref double ZAccuracy)
        {
            //NB: This will not work for traj files which loop around, we need to check for that when we do the initial read of the file
            if (Time < StartTimeD)
                return false;


            if (Time < m_storage[m_currRecordIdx].Time)
            {
                if (Time < m_storage[0].Time)
                {
                    //Temp for handling non sequential time stamps
                    gotoTime(Time);

                }
                else
                {
                    int temp_indx = m_storageList.FindIndex(tp => Time > tp.Time);
                    m_currRecordIdx = temp_indx != -1 && Time <= m_storage[temp_indx + 1].Time ? temp_indx : m_currRecordIdx;
                }
            }

            if (m_numRecords == 0 || Time > m_storage[m_currRecordIdx + 1].Time)
            {
                if (!gotoTime(Time))
                {
                    return false;
                }
            }

            if (m_currRecordIdx + 1 >= m_numRecords)
            {
                if (ReadRecords(SeekOrigin.Current) == 0)
                {
                    return false;
                }
                m_currRecordIdx = 0;
            }

            double relt = (Time - m_storage[m_currRecordIdx].Time) / (m_storage[m_currRecordIdx + 1].Time - m_storage[m_currRecordIdx].Time);
            bool linear_interp = m_currRecordIdx + 2 >= m_numRecords || m_currRecordIdx <= 0;
            Lat = linear_interp ? linearInterp(m_storage[m_currRecordIdx].Latitude, m_storage[m_currRecordIdx + 1].Latitude, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].Latitude, m_storage[m_currRecordIdx].Latitude, m_storage[m_currRecordIdx + 1].Latitude, m_storage[m_currRecordIdx + 2].Latitude, relt);
            Lon = linear_interp ? linearInterp(m_storage[m_currRecordIdx].Longitude, m_storage[m_currRecordIdx + 1].Longitude, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].Longitude, m_storage[m_currRecordIdx].Longitude, m_storage[m_currRecordIdx + 1].Longitude, m_storage[m_currRecordIdx + 2].Longitude, relt);
            Height = linear_interp ? linearInterp(m_storage[m_currRecordIdx].Height, m_storage[m_currRecordIdx + 1].Height, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].Height, m_storage[m_currRecordIdx].Height, m_storage[m_currRecordIdx + 1].Height, m_storage[m_currRecordIdx + 2].Height, relt);
            Heading = linear_interp ? linearAngularInterp(m_storage[m_currRecordIdx].Heading, m_storage[m_currRecordIdx + 1].Heading, relt) : cubicAngularInterp(m_storage[m_currRecordIdx - 1].Heading, m_storage[m_currRecordIdx].Heading, m_storage[m_currRecordIdx + 1].Heading, m_storage[m_currRecordIdx + 2].Heading, relt);
            Roll = linear_interp ? linearInterp(m_storage[m_currRecordIdx].Roll, m_storage[m_currRecordIdx + 1].Roll, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].Roll, m_storage[m_currRecordIdx].Roll, m_storage[m_currRecordIdx + 1].Roll, m_storage[m_currRecordIdx + 2].Roll, relt);
            Pitch = linear_interp ? linearInterp(m_storage[m_currRecordIdx].Pitch, m_storage[m_currRecordIdx + 1].Pitch, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].Pitch, m_storage[m_currRecordIdx].Pitch, m_storage[m_currRecordIdx + 1].Pitch, m_storage[m_currRecordIdx + 2].Pitch, relt);
            YAccuracy = linear_interp ? linearInterp(m_storage[m_currRecordIdx].YAccuracy, m_storage[m_currRecordIdx + 1].YAccuracy, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].YAccuracy, m_storage[m_currRecordIdx].YAccuracy, m_storage[m_currRecordIdx + 1].YAccuracy, m_storage[m_currRecordIdx + 2].YAccuracy, relt);
            XAccuracy = linear_interp ? linearInterp(m_storage[m_currRecordIdx].XAccuracy, m_storage[m_currRecordIdx + 1].XAccuracy, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].XAccuracy, m_storage[m_currRecordIdx].XAccuracy, m_storage[m_currRecordIdx + 1].XAccuracy, m_storage[m_currRecordIdx + 2].XAccuracy, relt);
            ZAccuracy = linear_interp ? linearInterp(m_storage[m_currRecordIdx].ZAccuracy, m_storage[m_currRecordIdx + 1].ZAccuracy, relt) : cubicInterp(m_storage[m_currRecordIdx - 1].ZAccuracy, m_storage[m_currRecordIdx].ZAccuracy, m_storage[m_currRecordIdx + 1].ZAccuracy, m_storage[m_currRecordIdx + 2].ZAccuracy, relt);
            return true;
        }

        private double linearAngularInterp(double x_0, double x_1, double relt)
        {
            if (x_1 - x_0 > 180)
            {
                x_0 = x_0 + 360;
            }
            else if (x_1 - x_0 < -180)
            {
                x_1 = x_1 + 360;
            }
            double angle = linearInterp(x_0, x_1, relt);
            angle = angle >= 360.0 ? angle - 360.0 : angle < 0 ? angle + 360.0 : angle;
            return angle;
        }

        private double cubicAngularInterp(double x_m1, double x_0, double x_p1, double x_p2, double relt)
        {
            if (x_p2 - x_p1 > 180)
            {
                x_p1 = x_p1 + 360;
            }
            if (x_p1 - x_0 > 180)
            {
                x_0 = x_0 + 360;
            }
            if (x_0 - x_m1 > 180)
            {
                x_m1 = x_m1 + 360;
            }

            if (x_0 - x_m1 < -180)
            {
                x_0 = x_0 + 360;
            }
            if (x_p1 - x_0 < -180)
            {
                x_p1 = x_p1 + 360;
            }
            if (x_p2 - x_p1 < -180)
            {
                x_p2 = x_p2 + 360;
            }

            double angle = cubicInterp(x_m1, x_0, x_p1, x_p2, relt);
            angle = angle >= 360.0 ? angle - 360.0 : angle < 0 ? angle + 360.0 : angle;
            return angle;
        }

        private double linearInterp(double x_0, double x_1, double relt)
        {
            double delta = (x_1 - x_0);
            return x_0 + relt * delta;
        }

        private double cubicInterp(double x_m1, double x_0, double x_p1, double x_p2, double relt)
        {
            double a = -0.5 * x_m1 + 1.5 * x_0 - 1.5 * x_p1 + 0.5 * x_p2;
            double b = x_m1 - 2.5 * x_0 + 2 * x_p1 - 0.5 * x_p2;
            double c = -0.5 * x_m1 + 0.5 * x_p1;
            double d = x_0;
            return a * relt * relt * relt + b * relt * relt + c * relt + d;
        }

        private bool gotoTime(double time)
        {
            bool skip = false;
            bool found = false;
            int recsRead = 0;
            bool stationary = false;
            bool triedRead = false;
            if (time < Convert.ToDouble(this.StartTime))
            {
                return false;
            }
            do
            {
                //First check through the values already read from the file
                for (int i = m_currRecordIdx; i < m_numRecords - 1; i++)
                {
                    if (m_trackOrientation)
                    {
                        //Track the orientation of the IMU
                        if (m_lastTrackedPosition.Time != 0)
                        {
                            //Tracked position exists, have we moved??
                            stationary = isStationaryPoint(i);
                            handleSwivelMount(i);
                        }
                        if (!stationary)
                            m_lastTrackedPosition = m_storage[i];
                    }
                    if (m_storage[i].Time <= time && m_storage[i + 1].Time >= time)
                    {
                        m_currRecordIdx = i;
                        found = true;
                        break;
                    }
                }
                if (!found)
                {
                    //The time we're looking for isn't in the current lot, read some more
                    if (m_numRecords > 0 && time < m_storage[0].Time && time >= m_firstTime)
                    {
                        //The time we're looking for is before the current set of records but it does exist within the file
                        // jump back to the beginning of the file & start again.
                        recsRead = ReadRecords(SeekOrigin.Begin);
                        m_lastTrackedPosition = new TrajectoryParams();
                    }
                    else
                    {
                        //The time isn't in the current set of records or no records have been read
                        if (!triedRead)
                        {
                            recsRead = ReadRecords(SeekOrigin.Current);
                            triedRead = true;
                        }
                        else
                        {
                            //Skip to time here instead of ReadData
                            if (!skip)
                            {
                                skipToTime(time);
                                skip = true;
                            }
                        }
                    }
                    m_currRecordIdx = 0;
                    if (recsRead == 0)
                    {
                        //End of the file, the time doesn't exist!
                        break;
                    }
                }

            } while (!found);
            return found;
        }

        private bool isStationaryPoint(int idx)
        {
            if (Math.Round(m_storage[idx].Latitude, 5) == Math.Round(m_lastTrackedPosition.Latitude, 5) && Math.Round(m_storage[idx].Longitude, 5) == Math.Round(m_lastTrackedPosition.Longitude, 5))
            {
                return true;
            }
            return false;
        }

        private void handleSwivelMount(int idx)
        {
            bool stationary = isStationaryPoint(idx);
            //If moving again, were we stationary long enough to spin the swivel mount around?
            if (m_storage[idx].Time - m_lastTrackedPosition.Time > 10 && !stationary)
            {
                //Possible swing of the swivel mount
                double angDiff = m_lastTrackedPosition.Heading - m_storage[idx].Heading;
                if (Math.Abs(angDiff) > 80)
                {
                    //Moved 90 degrees
                    m_orientation += angDiff < 0 ? -90 : 90;
                }
                else if (Math.Abs(angDiff) > 30)
                {
                    //Moved 45 degrees
                    m_orientation += angDiff < 0 ? -45 : 45;
                }
            }
        }

        public void Reset()
        {
            this.resetFile();
        }

        public List<TrajectoryTimeRange> getTurnarounds()
        {
            List<TrajectoryTimeRange> ranges = new List<TrajectoryTimeRange>();
            bool process = true;
            int numrecs;
            int index = 0;
            int detected = 0;
            double lastTime = 0;
            double x, y, z = 0;
            double[] xStore = new double[2];
            double[] yStore = new double[2];
            IntPtr srcCoord = Proj4.InitPlus("+proj=latlong +datum=WGS84");
            IntPtr dstCoord = Proj4.InitPlus("+proj=utm +ellps=WGS84 "
                                + (this.getInitialLatitude() < 0 ? "+south " : "")
                                + "+zone=" + Proj4.getUTMZone(this.getInitialLongitude()) + " +units=m +datum=WGS84");
            double heading, oldheading;
            heading = oldheading = 0;
            double startTime = 0;
            while (process)
            {
                numrecs = this.getData();
                for (int i = 0; i < numrecs; i++)
                {
                    if (index == 0 || this.getTime(i) - lastTime > 0.5)
                    {
                        lastTime = this.getTime(i);
                        x = Proj4.DegToRad(this.getLongitude(i));
                        y = Proj4.DegToRad(this.getLatitude(i));
                        int err = Proj4.Transform(srcCoord, dstCoord, 1, 1, ref x, ref y, ref z);

                        xStore[index % 2] = x;
                        yStore[index % 2] = y;

                        if (index > 0 && (xStore[index % 2] != xStore[(index - 1) % 2]) && (yStore[index % 2] != yStore[(index - 1) % 2]) &&
                            ((Math.Pow((xStore[index % 2] - xStore[(index - 1) % 2]), 2.0)) + (Math.Pow((yStore[index % 2] - yStore[(index - 1) % 2]), 2.0))) > 0.01)
                        {
                            oldheading = heading;
                            heading = Proj4.RadToDeg(Math.Atan2(xStore[index % 2] - xStore[(index - 1) % 2], yStore[index % 2] - yStore[(index - 1) % 2]));
                            if (heading < 0)
                            {
                                heading += 360;
                            }

                            if (index > 1)
                            {
                                if (Math.Abs(180 - Math.Abs(180 - Math.Abs(heading - oldheading))) > 150)
                                {
                                    detected++;
                                    if (detected % 2 == 0)
                                    {
                                        ranges.Add(new TrajectoryTimeRange(startTime, this.getTime(i)));
                                    }
                                    else
                                    {
                                        startTime = this.getTime(i);
                                    }
                                }
                            }
                        }

                        index++;
                    }
                }

                process = numrecs > 0;
            }


            return ranges;
        }

        public List<TrajectoryTimeRange> getStationary(double minTime)
        {
            List<TrajectoryTimeRange> ranges = new List<TrajectoryTimeRange>();
            bool process = true;
            int numrecs;
            int recsRead = 0;
            double lastTime = 0;
            double x, y = 0;
            double lastX = 0, lastY = 0;
            bool detected = false;
            double startTime = 0;
            ulong progress = 0;
            double range = Convert.ToDouble(this.EndTime) - Convert.ToDouble(this.StartTime);

            while (process)
            {
                numrecs = this.getData();
                for (int i = 0; i < numrecs; i += 150)
                {
                    lastTime = this.getTime(i);
                    x = this.getLongitude(i);
                    y = this.getLatitude(i);

                    if (!detected)
                    {
                        if ((x - lastX) > -0.00000002 && (x - lastX) < 0.00000002 && (y - lastY) > -0.00000005 && (y - lastY) < 0.00000005)
                        {
                            detected = true;
                            startTime = lastTime;
                        }
                    }
                    else
                    {
                        if ((x - lastX) > 0.0000001 || (y - lastY) > 0.00000005 || (x - lastX) < -0.0000001 || (y - lastY) < -0.00000005)
                        {
                            detected = false;
                            if ((lastTime - startTime) > minTime)
                            {
                                ranges.Add(new TrajectoryTimeRange(startTime, (lastTime - 1.5))); //Buffer of 1.5 seconds
                            }
                        }

                    }
                    lastX = x;
                    lastY = y;
                    recsRead += numrecs;

                    progress = Convert.ToUInt64(this.getTime(i) - Convert.ToDouble(this.StartTime));
                    progress = Convert.ToUInt64((progress / range) * 100);

                    if (m_progressUpdate != null)
                    {
                        m_progressUpdate(progress);
                    }

                }

                process = numrecs > 0;
            }

            return ranges;
        }

        public String FilePath
        {
            get { return m_filePath; }
        }

        public Boolean HasAccuracy
        {
            get { return m_hasAccuracy; }
        }
    }
}
