﻿namespace SALMTesting.SLAMFiles
{

    public class TrajectoryTimeRange
    {
        public double TimeFrom;
        public double TimeTo;
        public TrajectoryTimeRange()
        {
            TimeTo = TimeFrom = 0;

        }
        public TrajectoryTimeRange(double from, double to)
        {
            TimeFrom = from;
            TimeTo = to;
        }
    }
}
