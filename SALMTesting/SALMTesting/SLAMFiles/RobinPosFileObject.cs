﻿namespace SALMTesting.SLAMFiles
{
    public class RobinPosFileObject
    {
        public double T { get; }
        public Point3D Position { get; }
        public double Roll { get; }
        public double Pitch { get; }
        public double Heading { get; }
        public double Rotation { get; private set; }
        public double Dx { get;private set; }
        public double Dy { get;private set; } 
        public RobinPosFileObject(double time, double x, double y, double z, double roll, double pitch, double heading)
        {
            T = time;
            Position = new Point3D
            {
                X = x,
                Y = y,
                Z = z
            };
            Roll = roll;
            Pitch = pitch;
            Heading = heading;
        }

        public void SetDx(double dx)
        {
            Dx = dx;
        }
        public void SetDy(double dy)
        {
            Dy = dy;
        }
        public void SetRotation(double rotation)
        {
            Rotation = rotation;
        }
    }
}
