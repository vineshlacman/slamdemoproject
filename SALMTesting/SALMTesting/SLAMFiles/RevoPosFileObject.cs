﻿namespace SALMTesting.SLAMFiles
{
    public class RevoPosFileObject
    {
        public double T { get; }
        public Point3D Position { get; }
        public double Q1 { get; }
        public double Q2 { get; }
        public double Q3 { get; }
        public double Q4 { get; }
        public double Rotation { get; private set; }
        public double Dx { get; private set; }
        public double Dy { get; private set; }
        public RevoPosFileObject(double time, double x, double y, double z, double q1, double q2, double q3, double q4)
        {
            T = time;
            Position = new Point3D
            {
                X = x,
                Y = y,
                Z = z
            };
            Q1 = q1;
            Q2 = q2;
            Q3 = q3;
            Q4 = q4;
        }

        public void SetDx(double dx)
        {
            Dx = dx;
        }
        public void SetDy(double dy)
        {
            Dy = dy;
        }
        public void SetRotation(double rotation)
        {
            Rotation = rotation;
        }
    }
}
