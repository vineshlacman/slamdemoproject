﻿using NUnit.Framework;
using SALMTesting.SLAMFiles;
using System.Collections.Generic;
using System;

namespace SALMTesting.NUnit
{
    [TestFixture]
    class TaskRevoPOSFixture
    {
        class Comparitor : IComparer<Point3D>, System.Collections.IComparer
        {
            public int Compare(object x, object y)
            {
                return Compare((Point3D)x, (Point3D)y);
            }

            public int Compare(Point3D left, Point3D right)
            {
                if (Math.Abs(left.X - right.X) > 10e-4)
                {
                    return 1;
                }
                if (Math.Abs(left.Y - right.Y) > 10e-4)
                {
                    return 1;
                }
                if (Math.Abs(left.Z - right.Z) > 10e-4)
                {
                    return 1;
                }
                return 0;
            }
        }
        [Test]
        public void RotatePoint()
        {
            const double radiansRotate = -0.60833102;
            List<Point3D> points = new List<Point3D>
            {
                new Point3D { X=648477.2195, Y=5769911.2520 }, //1 (root)
                new Point3D { X=648477.4299, Y=5769911.0343 }, //20
                new Point3D { X=648478.2965, Y=5769923.2351 } //21019   648478.2965 5769923.2351
            };

            List<Point3D> expected = new List<Point3D>
            {
                new Point3D { X=648477.2195, Y=5769911.2520 }, //1 (root)
                new Point3D { X=648477.2677, Y=5769910.9531 }, //20 648484.9516 5769920.4699
                new Point3D { X=648484.9516, Y=5769920.4699 } //21019   648484.9516 5769920.4699

            };
            SLAMMath.Rotate(points, radiansRotate);
            CollectionAssert.AreEqual(expected, points, new Comparitor());
        }
    }
}
